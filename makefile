all: run

run:
	java src.MachineViewer

compile:
	javac src/*.java

clean:
	rm -f src/*.class
