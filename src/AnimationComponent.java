package src;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JComponent;

public class AnimationComponent extends JComponent implements Observer{
	Color color;
	MachineViewer machineViewer;
	Model model;
	
	public AnimationComponent(MachineViewer machineViewer){
		this.machineViewer = machineViewer;
		model = machineViewer.getModel();
		color = Color.black;
		this.setOpaque(false);
//		setBackground(Color.black);
	}
	
	@Override
	public void paintComponent(Graphics g){
		setBackground(Color.black);
		Graphics2D g2 = (Graphics2D) g;
		setBackground(Color.black);
		g2.setColor(color);
		Ellipse2D ellipse ;


		for(int i = 0 ; i < model.points.size(); i++){
			if(i+1<model.points.size())
			g2.drawLine(model.points.get(i).x, model.points.get(i).y, model.points.get(i+1).x, model.points.get(i+1).y);
		}
		
		for(Tail tail : model.tails){
			if(tail.getTargetRadius() >0 ){
				ellipse = new Ellipse2D.Double(
					tail.getLocation().x - tail.getTargetRadius(),
					tail.getLocation().y - tail.getTargetRadius(),
					tail.getTargetRadius() , tail.getTargetRadius() 
				);
				g2.setColor(tail.getColor());	
				g2.draw(ellipse);
				g2.fill(ellipse);	
			}
		}
		
	}

	@Override
	public void update(Observable o, Object arg) {
		//if(arg!=null)
		this.color = machineViewer.getModel().getColor(); 
		if(arg != null){
			if(arg instanceof Color){
				this.color = (Color) arg;
			}
		}
		repaint();
	}
	
	public void setColor(Color color){
//		this.color = color;
//		repaint();
	}
}
