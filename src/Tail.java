package src;


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.*;
import javax.swing.JComponent;
import java.awt.Rectangle;
import java.awt.Point;
import java.awt.Color;



public class Tail{
	
	private Point target   = new Point(0,0);
	private Point location = new Point(0,0);
	private Point previous = new Point(0,0);	
	private Point randTarget = new Point(0,0);
	int targetRadius = 30;
	
	double tailSpeed = .20;
	
	boolean darkMode = false;
	
	int R = 0;
	int G = 0;
	int B = 0;



	public Tail(){		}
	public Tail(Point p){ this.location = p;} 


	public void setTarget(Point p){
		this.target = new Point(p);
	}


	public Point getTarget(){
		return this.target;
	}

	public Point getLocation(){
		return location;
	}


	public Point distanceTo( Point b){
		Point retval = new Point();
		retval.x = (int) (b.x - location.x);
		retval.y =(int) (b.y - location.y);
		R = Math.abs(retval.x) * 1 ;
		G = Math.abs(retval.y) * 2;	
	return retval;
	}	


	public void moveToward(Point p){
		Point vectorDist = distanceTo(p);		
		moveThingBy((int) (vectorDist.x * tailSpeed), (int) (vectorDist.y * tailSpeed) );
	}

	
	public void moveThingBy(int x, int y){
		previous = new Point(location);
		location.x += x;
		location.y +=y;	
	}

	
	public void setTargetRadius(int rad){
		targetRadius = rad;
	}


	public int getTargetRadius(){
		return targetRadius;
	}


	public Point getPrevious(){
		return previous;
	}

	public void setRandTarget(Point p){
		this.randTarget= new Point(p);
	}

	public Point getRandTarget(){
		return this.randTarget ; 
	}
	

	public Color getColor(){		
		if (R < 0) {R = 0; 	} else if (R > 255) {R = 255;}	
		if (G < 0) {G = 0;	} else if (G > 255) {G = 255;}	
		if(!darkMode){B = 100;}		
		Color col = new Color(R, G, B);	

	return col;	
	}
	
	void setTailSpeed(double speed){
		this.tailSpeed = speed;
	}
	
	
}
