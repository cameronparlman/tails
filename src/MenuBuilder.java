package src;

import javax.swing.JMenu;

public class MenuBuilder {

	private MachineViewer machineViewer;

	public MenuBuilder(MachineViewer machineViewer) {
		this.machineViewer = machineViewer;
	}

	public JMenu createFileMenu(){
		JMenu returnMenu = new JMenu("File");
		return returnMenu;
	}
	
	public JMenu createExecuteMenu(){
		JMenu menu = new JMenu("Execute");
		return menu;
	}
	
}
