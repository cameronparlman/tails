package src;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JComponent;
import javax.swing.JPanel;

public class AnimationPanel implements Observer {
	MachineViewer machineViewer;
	JPanel panel;
	AnimationComponent animationComponent;
	Color backgroundColor = Color.white;
	Color circleColor = Color.black;
	
	public AnimationPanel(MachineViewer machineViewer){
		this.machineViewer = machineViewer;
	}

	
	public JComponent createAnimationDisplay(){
		 panel = new JPanel();
		 animationComponent = new AnimationComponent(machineViewer);

		 panel.add(animationComponent);

		return animationComponent;
	}
	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Color){
			System.out.println("hit");
			setBackground((Color)arg);
		
		}
	}
	
	public  void setBackground(Color color){
		panel.setBackground(color);
	}
	public AnimationComponent getAnimationComponent(){
		return animationComponent;
	}
}
