package src;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Observable;
import java.awt.PointerInfo;
import java.awt.MouseInfo;


public class Model extends Observable {
	int tickTimer = 500; // 1/2 second 500 milseconds
	Point origin = new Point(250,250);

	Tail tailHead;
	LinkedList<Tail> linkedTail= new LinkedList<Tail>();
	ArrayList<Tail> tails = new ArrayList<>();
	ArrayList<Point> points = new ArrayList<>();

	Point leaderLocation = new Point(100,100);

	int count = 0;
	int numTails = 30;
	int RADIUS = 60;
	Color color;
	
	public enum State{
		CURVE, CIRCLE, RANDOM, DIRECTION, POINTER, AVOID
	}
	State state = State.POINTER;

	
	/**
	model constructor 
	**/	
	public Model(){
//		addObserver(animationComponent)
		color = Color.white;
		tailHead = new Tail(leaderLocation);
		
		makeTails();
	}
	
	/**
	set state	
	**/
	public void setState(int i){
		switch(i){
		case 0:
			state = State.CURVE;	
			break;
		case 1:
			state = State.CIRCLE;	
			break;
		case 2:
			state = State.RANDOM;	
			break;
		case 3:
			state = State.DIRECTION;	
			break;
		case 4:
			state = State.POINTER;	
			break;
		case 5:
			state = State.AVOID;	
			break;
		}
		//System.out.println("stateUPDATE: " + i);
	}

	/**
	set color of line?	
	**/
	public void setColor(Color color){
		this.color = color;
//		setChanged();   machineView notifies observers....
//		notifyObservers();
	}

	/**
	return the color of the lines?
	**/
	public Color getColor(){
		return color;
	}
	
	/**
	return tickTimer (speed of clock)
	**/
	int getTimer(){
		return tickTimer;
	}


	/**
	set tickTimer (speed of clock) 	
	**/
	void setTickTimer(int tickTimer){
		this.tickTimer = tickTimer;
	}

	/**
	each timestep do something
	**/	
	void step(){
		count++;
		
		if(State.CURVE == state){
			moveLeaderCurve(count);	
			//System.out.println("state Curve");
		}
		else if(State.CIRCLE == state){
			moveLeaderCurve(count);	
			//System.out.println("state Circle");
		}
		else if(State.RANDOM == state){
			moveLeaderCurve(count);	
			//System.out.println("state Random");
		}
		else if(State.DIRECTION == state){
			moveLeaderCurve(count);	
			//System.out.println("state Direction");
		}
		else if(State.POINTER == state){
			moveLeaderPointer();
			//System.out.println("state Pointer");
		}
		else if(State.AVOID == state){
			moveLeaderCurve(count);	
			//System.out.println("state avoid");
		}
		else{
			moveLeaderCurve(count);	
			//System.out.println("state default");
		}
			//moveLeaderCurve(count);	

		tailHead.moveToward(leaderLocation);
		moveTails();

		if(count < 30){
			points.add(new Point(leaderLocation));
		}
		
	
	}
	/**
	move the leader 
	**/	
	void moveLeaderCurve(int count){
				
		double a = 200;	
		double f = 200;
		double b = 200;
		int divisor1 = tickTimer/2;  	
		int divisor2 = 2;  	
		int divisor3 = 4;  	
		/*
		leaderLocation.x = (int)(a * (Math.sin(count) + Math.cos(count)/divisor2 + Math.sin(count)/divisor3) + origin.x);
		leaderLocation.y = (int)(a * (Math.cos(count) + Math.sin(count)/divisor2 + Math.cos(count)/divisor3) + origin.y);
		*/
		leaderLocation.x =((int)((Math.pow(a,2) + Math.pow(f,2) * Math.pow(Math.sin(count/divisor1),2) )*Math.cos(count/divisor1))
				     	/(int)a + origin.x);	
		leaderLocation.y =((int)((Math.pow(a,2) - 2 * Math.pow(f,2) + Math.pow(f,2)*Math.pow(Math.sin(count/divisor1),2)
					 )*Math.sin(count/divisor1))/(int)b + origin.y);	

	}

	void moveLeaderPointer(){
		leaderLocation = MouseInfo.getPointerInfo().getLocation();
	}



	/**
	each tail.
	**/
	void moveTails(){
		tails.get(0).moveToward(leaderLocation);
		for(int i = 1 ; i < tails.size(); i++){
			tails.get(i).moveToward(tails.get(i-1).getPrevious());//move tails
	//		linkedTail.get(i).moveToward(tails.get(i-1).getPrevious());//move linkedTail
			
		}
	}

	/**
	Make "RADIUS" # of Tails
	**/	
	public void makeTails(){ 
		int targetRadius = (int) RADIUS;
		for(int i = 0 ; i < numTails ; i++){
			targetRadius = (int) (RADIUS /(.65 * (i+2)));
			if(targetRadius > 0){
					Tail newTail = new Tail(new Point(origin.x + 10 * i, origin.y + 10*i));
					if(i == 0) { 
						newTail.setTarget(leaderLocation.getLocation());
						newTail.setTargetRadius((int) (RADIUS /(.65 * 2)));
					}
					else{
						newTail.setTarget(tails.get(i-1).getLocation()); 
						newTail.setTargetRadius((int)(RADIUS / (.65 * (i+2) ))); 
					}	
				tails.add(newTail);	
				linkedTail.add(newTail);
			} // 
		}	
		System.out.println(tails.size());
}
	
}
