package src;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MachineViewer extends Observable{
	Model model;
	ControlPanel controlPanel;
	AnimationPanel animationPanel;
	AnimationComponent animationConponent;
	MenuBuilder menuBuilder;
	JFrame frame;
	javax.swing.Timer timer;
	
	public MachineViewer(){
		createAndShowGUI();
		addObserver(getAnimationPanel().getAnimationComponent());

	}
	
	public static void main(String[] args){
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new MachineViewer();
			}
		});
	}

	private void createAndShowGUI() {
		model = new Model();
		controlPanel = new ControlPanel(this);
		animationPanel = new AnimationPanel(this);
		menuBuilder = new MenuBuilder(this);
		frame = new JFrame("Tails Sim");
		Container content = frame.getContentPane();
		content.setLayout(new BorderLayout(1,1));
		content.setBackground(Color.black);
		frame.setSize(1200,600);
		JPanel center = new JPanel();
		center.setLayout(new GridLayout(1,1));
		
		frame.add(controlPanel.createControlDisplay(), BorderLayout.LINE_START);
		center.setBackground(Color.black);
		frame.add(center, BorderLayout.CENTER);
		center.add(animationPanel.createAnimationDisplay());
		
		JMenuBar bar = new JMenuBar();
		frame.setJMenuBar(bar);
		bar.add(menuBuilder.createFileMenu());
		bar.add(menuBuilder.createExecuteMenu());
		bar.setBackground(Color.LIGHT_GRAY);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(WindowListenerFactory.windowClosingFactory(e -> exit()));
		notifyObservers();
		makeTimer(50);
		frame.setVisible(true);
	}
	
	public void exit() {
		int decision = JOptionPane.showConfirmDialog(frame, "Do you really wish to exit", "Confirmation", JOptionPane.YES_NO_OPTION);
			if(decision == JOptionPane.YES_OPTION){
				System.exit(0);
			}
	}
	
	void step(){
		model.step();
		notfiy(null);
	}
	
	public void setAnimationPanelColor(Color color){
		 animationPanel.setBackground(color);
	}
	public AnimationPanel getAnimationPanel(){
		return animationPanel;
	}
	public void notfiy(Object arg){
		setChanged();
		notifyObservers(arg);
	}

	public Model getModel() {
		return model;
	}
	
	public void makeTimer(int tickTime){
		model.setTickTimer(tickTime);
		if(timer != null){
			if(timer.isRunning()){
				timer.stop();
			}
		}
		timer = new javax.swing.Timer(tickTime, e-> step());
		timer.start();
	}

}
