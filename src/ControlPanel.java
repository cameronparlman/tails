package src;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Observable;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;

public class ControlPanel extends Observable {
	MachineViewer machineViewer;
	Color color = Color.white;
	String red = "";
	
	
	public ControlPanel(MachineViewer machineViewer){
		this.machineViewer = machineViewer;
	}
	
	public JComponent createControlDisplay(){
		JPanel returnPanel = new JPanel();
		JPanel panel = new JPanel();
		returnPanel.setPreferredSize(new Dimension(200, 150));
		panel.setLayout(new GridLayout(40,1));
		
		JSlider redSlider = new JSlider(0, 255, 125);
		JSlider greenSlider = new JSlider(0, 255, 125);
		JSlider blueSlider = new JSlider(0, 255, 125);
		JSlider timerSlider = new JSlider(4,100,50);
		JSlider tailSpeedSlider =  new JSlider (0,100,20);
		JSlider stateSlider  = new JSlider(0,5,0);	
		
		redSlider.setOpaque(false);
		greenSlider.setOpaque(false);
		blueSlider.setOpaque(false);
		timerSlider.setOpaque(false);
		tailSpeedSlider.setOpaque(false);
		stateSlider.setOpaque(false);
		
		ChangeListener listener = makeColorListener(redSlider, greenSlider, blueSlider);
		redSlider.addChangeListener(listener);
		greenSlider.addChangeListener(listener);
		blueSlider.addChangeListener(listener);
		timerSlider.addChangeListener(e -> {
					machineViewer.makeTimer(timerSlider.getValue());
					machineViewer.getModel().setTickTimer(timerSlider.getValue());
					machineViewer.notfiy(null);	
				});
		tailSpeedSlider.addChangeListener(makeSpeedListener(tailSpeedSlider));
		stateSlider.addChangeListener(e->{
					machineViewer.getModel().setState(stateSlider.getValue());
					//System.out.println(stateSlider.getValue());
					//machineViewer.notfiy(null);
					//notifyObservers();
				});
		


		panel.add(new JLabel("State slider"));
		panel.add(stateSlider);
		panel.add(new JLabel(""));
		panel.add(new JLabel("RGB"));
		panel.add(redSlider);
//		panel.add(new JLabel("Green"));
		panel.add(greenSlider);
//		panel.add(new JLabel("Blue"));
		panel.add(blueSlider);
		panel.add(new JLabel(""));
		panel.add(new JLabel("Timer"));
		panel.add(timerSlider);
		panel.add(new JLabel("Tail Speed"));
		panel.add(tailSpeedSlider);
		
		panel.setBackground(Color.LIGHT_GRAY);
		returnPanel.setBackground(Color.LIGHT_GRAY);
		JScrollPane scroller = new JScrollPane(panel);
		returnPanel.add(scroller);
		return returnPanel;
	}
	
	public ChangeListener makeColorListener(JSlider redSlider, JSlider greenSlider, JSlider blueSlider){
		ChangeListener listener = (e -> {
			color = new Color(redSlider.getValue(), greenSlider.getValue(),  blueSlider.getValue());
			machineViewer.getModel().setColor(color);			
			machineViewer.notfiy(null);
			});
		return listener;
	}

	//should every tail get its speed from a single location? or does having indivial speed values have any interesting benifit? 	
	ChangeListener makeSpeedListener(JSlider speedSlider){
		ChangeListener listener = (e -> {
				for(Tail tail: machineViewer.getModel().tails){
					tail.setTailSpeed( (double)(speedSlider.getValue()) /100);
				}
			});
		return listener;
	}

}
